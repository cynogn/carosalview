package com.example.category;

/**
 * WorldNews contains the feed link to the this Category
 * 
 * @author GAUTAM BALASUBRAMANIAN
 * 
 * @Version 1
 */
public class WorldNews {
	/**
	 * mName contains the Tag
	 */
	public static String mName = "WorldNews";
	/**
	 * mLink contains the link to the Tag
	 */
	public static String mLink = "http://feeds.feedburner.com/ndtv/TqgX";

}
