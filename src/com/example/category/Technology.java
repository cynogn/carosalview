package com.example.category;

/**
 * Technology contains the feed link to the this Category
 * 
 * @author GAUTAM BALASUBRAMANIAN
 * 
 * @Version 1
 */
public class Technology {
	/**
	 * mName contains the Tag
	 */
	public static String mName = "Technology";
	/**
	 * mLink contains the link to the Tag
	 */
	public static String mLink = "http://feeds.feedburner.com/NDTV-Tech";

}
