package com.example.category;

/**
 * Cricket contains the feed link to the this Category
 * 
 * @author GAUTAM BALASUBRAMANIAN
 * 
 * @Version 1
 */
public class Cricket {
	/**
	 * mName contains the Tag
	 */
	public static String mName = "Cricket";
	/**
	 * mLink contains the link to the Tag
	 */
	public static String mLink = "http://feeds.feedburner.com/NDTV-Cricket";

}
