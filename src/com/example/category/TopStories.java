package com.example.category;

/**
 * TopStories contains the feed link to the this Category
 * 
 * @author GAUTAM BALASUBRAMANIAN
 * 
 * @Version 1
 */
public class TopStories {
	/**
	 * mName contains the Tag
	 */
	public static String mName = "TopStories";
	/**
	 * mLink contains the link to the Tag
	 */
	public static String[] mLink = {
			"http://feeds.feedburner.com/NdtvNews-TopStories",
			"http://feeds.feedburner.com/NdtvNews-TopStories",
			"http://feeds.feedburner.com/NdtvNews-TopStories" };

}
