package com.example.category;

/**
 * LatestSorties contains the feed link to the this Category
 * 
 * @author GAUTAM BALASUBRAMANIAN
 * 
 * @Version 1
 */
public class LatestSorties {
	/**
	 * mName contains the Tag
	 */
	public static String mName = "LatestSorties";
	/**
	 * mLink contains the link to the Tag
	 */
	public static String mLink = "http://feeds.feedburner.com/NDTV-LatestNews";

}
