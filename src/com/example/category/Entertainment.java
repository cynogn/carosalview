package com.example.category;

/**
 * Entertainment contains the feed link to the this Category
 * 
 * @author GAUTAM BALASUBRAMANIAN
 * 
 * @Version 1
 */
public class Entertainment {
	/**
	 * mName contains the Tag
	 */
	public static String mName = "Entertainment";
	/**
	 * mLink contains the link to the Tag
	 */
	public static String[] mLink = { "http://feeds.feedburner.com/NDTV-Ent" };

}
